package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/gelora-app/auth-service/pkg/container"
)

func RunServer() *cobra.Command {
	appCmd := &cobra.Command{
		Use:   "auth-service",
		Short: "Start Auth Service",
		Long:  "Start Auth Service",
		Run:   AuthServer,
	}

	return appCmd
}

func AuthServer(cmd *cobra.Command, args []string) {
	container.NewContainer()
}
