package main

import (
	"os"

	"gitlab.com/gelora-app/auth-service/cmd"
)

func main() {
	app := cmd.RunServer()
	if err := app.Execute(); err != nil {
		os.Exit(1)
	}
}
