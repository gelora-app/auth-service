CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(50) NOT NULL,
    full_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NULL,
    phone_number VARCHAR(15) NULL,
    password VARCHAR(100) NOT NULL,
    verified_email BOOLEAN,
    verified_phone_number BOOLEAN,
    created_at TIMESTAMPTZ,
    updated_at TIMESTAMPTZ,
    PRIMARY KEY ("id")
);