module gitlab.com/gelora-app/auth-service

go 1.16

require (
	github.com/DataDog/zstd v1.3.6-0.20190409195224-796139022798 // indirect
	github.com/Shopify/sarama v1.29.1 // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/go-redis/redis/v8 v8.11.0 // indirect
	github.com/gofiber/fiber/v2 v2.15.0
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/spf13/cobra v1.2.1
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.2.3 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12
)
