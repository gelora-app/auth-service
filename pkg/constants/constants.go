package constants

import "errors"

const (
	HTTP_STATUS_OK_CODE                    = 200
	HTTP_STATUS_CREATED_CODE               = 201
	HTTP_STATUS_BAD_REQUEST_CODE           = 400
	HTTP_STATUS_NOT_FOUND_CODE             = 404
	HTTP_STATUS_INTERNAL_SERVER_ERROR_CODE = 500
)

const (
	HTTP_STATUS_OK                    = "OK"
	HTTP_STATUS_CREATED               = "CREATED"
	HTTP_STATUS_BAD_REQUEST           = "BAD_REQUEST"
	HTTP_STATUS_NOT_FOUND             = "NOT_FOUND"
	HTTP_STATUS_INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR"
)

const (
	CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
)

const (
	TOPIC_SEND_EMAIL_VERIFICATION = "EVENT-SEND-EMAIL-VERIFICATION"
)

var (
	UserAlreadyExistsWithThisEmailError = errors.New("user already exists with this email")
	UserNotFoundError                   = errors.New("user not found")
	EmailTokenNotFoundError             = errors.New("email token not found")
	InvalidEmailTokenError              = errors.New("invalid email token")
)
