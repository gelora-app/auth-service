package exception

func Panic(err error) {
	if err != nil {
		panic(err)
	}
}

func ValidationErrorHandler(err error) {
	if err != nil {
		panic(ValidationError{
			Message: err.Error(),
		})
	}
}
