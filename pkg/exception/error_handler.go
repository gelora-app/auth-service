package exception

import (
	"errors"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
)

func ErrorHandler(ctx *fiber.Ctx, err error) error {
	_, validationError := err.(ValidationError)
	userAlreadyExistsWithThisEmailError := errors.Is(err, constants.UserAlreadyExistsWithThisEmailError)
	emailTokenNotFoundError := errors.Is(err, constants.EmailTokenNotFoundError)
	invalidEmailTokenError := errors.Is(err, constants.InvalidEmailTokenError)

	if validationError {
		return ctx.Status(constants.HTTP_STATUS_BAD_REQUEST_CODE).JSON(dto.GeneralResponse{
			Code:    constants.HTTP_STATUS_BAD_REQUEST_CODE,
			Status:  constants.HTTP_STATUS_BAD_REQUEST,
			Message: strings.ToUpper(err.Error()),
		})
	} else if userAlreadyExistsWithThisEmailError {
		return ctx.Status(constants.HTTP_STATUS_BAD_REQUEST_CODE).JSON(dto.GeneralResponse{
			Code:    constants.HTTP_STATUS_BAD_REQUEST_CODE,
			Status:  constants.HTTP_STATUS_BAD_REQUEST,
			Message: strings.ToUpper(err.Error()),
		})
	} else if emailTokenNotFoundError {
		return ctx.Status(constants.HTTP_STATUS_NOT_FOUND_CODE).JSON(dto.GeneralResponse{
			Code:    constants.HTTP_STATUS_NOT_FOUND_CODE,
			Status:  constants.HTTP_STATUS_NOT_FOUND,
			Message: strings.ToUpper(err.Error()),
		})
	} else if invalidEmailTokenError {
		return ctx.Status(constants.HTTP_STATUS_BAD_REQUEST_CODE).JSON(dto.GeneralResponse{
			Code:    constants.HTTP_STATUS_BAD_REQUEST_CODE,
			Status:  constants.HTTP_STATUS_BAD_REQUEST,
			Message: strings.ToUpper(err.Error()),
		})
	}

	return ctx.Status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR_CODE).JSON(dto.GeneralResponse{
		Code:    constants.HTTP_STATUS_INTERNAL_SERVER_ERROR_CODE,
		Status:  constants.HTTP_STATUS_INTERNAL_SERVER_ERROR,
		Message: strings.ToUpper(err.Error()),
	})
}
