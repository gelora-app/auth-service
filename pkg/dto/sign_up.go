package dto

type SignUpWithEmailRequest struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type SignUpWithEmailResponse struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
}
