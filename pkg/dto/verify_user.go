package dto

import "time"

type VerifyEmailRequest struct {
	Token string
}

type VerifyEmailResponse struct {
	VerifiedEmail bool      `json:"verifiedEmail"`
	UpdatedAt     time.Time `json:"updatedAt"`
}
