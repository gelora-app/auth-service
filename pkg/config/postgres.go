package config

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gelora-app/auth-service/pkg/exception"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPostgresDatabase(config Config) *gorm.DB {
	host := config.Get("POSTGRES_HOST")
	port := config.Get("POSTGRES_PORT")
	user := config.Get("POSTGRES_USER")
	password := config.Get("POSTGRES_PASSWORD")
	db := config.Get("POSTGRES_DBNAME")
	ssl := config.Get("POSTGRES_SSLMODE")
	timeZone := config.Get("POSTGRES_TIMEZONE")

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s TimeZone=%s", host, port, user, password, db, ssl, timeZone)
	connection, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	exception.Panic(err)

	return connection
}

func NewPostgresContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 5*time.Second)
}
