package config

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

func NewRedisDatabase(config Config) *redis.Client {
	host := config.Get("REDIS_HOST")
	port := config.Get("REDIS_PORT")
	password := config.Get("REDIS_PASSWORD")

	address := fmt.Sprintf("%s:%s", host, port)
	connection := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
	})

	return connection
}

func NewRedisContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 5*time.Second)
}
