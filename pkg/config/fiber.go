package config

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
)

func NewFiberConfig() fiber.Config {
	return fiber.Config{
		ErrorHandler: exception.ErrorHandler,
	}
}
