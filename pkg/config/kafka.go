package config

import (
	"time"

	"github.com/Shopify/sarama"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
)

func NewKafkaProducer(config Config) sarama.SyncProducer {
	kafkaConfig := sarama.NewConfig()
	kafkaConfig.Producer.Return.Successes = true
	kafkaConfig.Net.WriteTimeout = 5 * time.Second
	kafkaConfig.Producer.Retry.Max = 0

	address := []string{config.Get("KAFKA_ADDRESS")}
	producer, err := sarama.NewSyncProducer(address, kafkaConfig)
	exception.Panic(err)

	return producer
}
