package config

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
)

type Config interface {
	Get(key string) string
}

type ConfigImpl struct{}

func (config *ConfigImpl) Get(key string) string {
	return os.Getenv(key)
}

func New(filenames ...string) Config {
	err := godotenv.Load(filenames...)
	exception.Panic(err)
	return &ConfigImpl{}
}
