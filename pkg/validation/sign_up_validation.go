package validation

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
)

func SignUpWithEmailValidate(request dto.SignUpWithEmailRequest) {
	err := validation.ValidateStruct(
		&request,
		validation.Field(&request.FullName, validation.Required),
		validation.Field(&request.Email, validation.Required),
		validation.Field(&request.Password, validation.Required),
	)
	exception.ValidationErrorHandler(err)
}
