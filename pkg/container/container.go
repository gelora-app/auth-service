package container

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"gitlab.com/gelora-app/auth-service/pkg/config"
	"gitlab.com/gelora-app/auth-service/pkg/container/sub"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
)

func NewContainer() error {
	configuration := config.New()
	postgres := config.NewPostgresDatabase(configuration)
	redis := config.NewRedisDatabase(configuration)
	kafkaProducer := config.NewKafkaProducer(configuration)

	signUpContainer := sub.NewSignUpContainer(postgres, redis, kafkaProducer)
	verifyUserContainer := sub.NewVerifyUserContainer(postgres, redis)

	fiber := fiber.New(config.NewFiberConfig())
	fiber.Use(recover.New())

	signUpContainer.Route(fiber)
	verifyUserContainer.Route(fiber)

	err := fiber.Listen(":8080")
	exception.Panic(err)

	return nil
}
