package sub

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/gelora-app/auth-service/pkg/controller"
	postgresRepositoryImpl "gitlab.com/gelora-app/auth-service/pkg/repository/postgres/impl"
	redisRepositoryImpl "gitlab.com/gelora-app/auth-service/pkg/repository/redis/impl"
	serviceImpl "gitlab.com/gelora-app/auth-service/pkg/service/impl"
	"gorm.io/gorm"
)

func NewVerifyUserContainer(postgres *gorm.DB, redis *redis.Client) controller.VerifyUserController {
	userPostgresRepository := postgresRepositoryImpl.NewUserRepository(postgres)
	tokenEmailVerificationRedisRepository := redisRepositoryImpl.NewTokenEmailVerification(redis)
	verifyUserService := serviceImpl.NewVerifyUserService(&userPostgresRepository, &tokenEmailVerificationRedisRepository)
	verifyUserController := controller.NewVerifyUserController(&verifyUserService)

	return verifyUserController
}
