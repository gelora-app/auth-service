package sub

import (
	"github.com/Shopify/sarama"
	"github.com/go-redis/redis/v8"
	"gitlab.com/gelora-app/auth-service/pkg/controller"
	kafkaProducerRepositoryImpl "gitlab.com/gelora-app/auth-service/pkg/repository/kafka/impl"
	postgresRepositoryImpl "gitlab.com/gelora-app/auth-service/pkg/repository/postgres/impl"
	redisRepositoryImpl "gitlab.com/gelora-app/auth-service/pkg/repository/redis/impl"
	serviceImpl "gitlab.com/gelora-app/auth-service/pkg/service/impl"
	"gorm.io/gorm"
)

func NewSignUpContainer(postgres *gorm.DB, redis *redis.Client, kafkaProducer sarama.SyncProducer) controller.SignUpController {
	userPostgresRepository := postgresRepositoryImpl.NewUserRepository(postgres)
	tokenEmailVerificationRedisRepository := redisRepositoryImpl.NewTokenEmailVerification(redis)
	kafkaProducerRepository := kafkaProducerRepositoryImpl.NewKafkaProducerRepositoryImpl(kafkaProducer)
	signUpService := serviceImpl.NewSignUpService(&userPostgresRepository, &tokenEmailVerificationRedisRepository, &kafkaProducerRepository)
	signUpController := controller.NewSignUpController(&signUpService)

	return signUpController
}
