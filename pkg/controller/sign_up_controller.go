package controller

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	"gitlab.com/gelora-app/auth-service/pkg/service"
)

type SignUpController struct {
	SignUpService service.SignUpService
}

func NewSignUpController(signUpService *service.SignUpService) SignUpController {
	return SignUpController{SignUpService: *signUpService}
}

func (controller *SignUpController) Route(app *fiber.App) {
	app.Post("/api/auth/signUpWithEmail", controller.SignUpWithEmail)
}

func (controller *SignUpController) SignUpWithEmail(ctx *fiber.Ctx) error {
	var request dto.SignUpWithEmailRequest
	err := ctx.BodyParser(&request)
	exception.Panic(err)

	response := controller.SignUpService.SignUpWithEmail(request)

	return ctx.Status(constants.HTTP_STATUS_CREATED_CODE).JSON(dto.GeneralResponse{
		Code:   constants.HTTP_STATUS_CREATED_CODE,
		Status: constants.HTTP_STATUS_CREATED,
		Data:   response,
	})
}
