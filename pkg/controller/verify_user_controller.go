package controller

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
	"gitlab.com/gelora-app/auth-service/pkg/service"
)

type VerifyUserController struct {
	VerifyUserService service.VerifyUserService
}

func NewVerifyUserController(verifyUserService *service.VerifyUserService) VerifyUserController {
	return VerifyUserController{VerifyUserService: *verifyUserService}
}

func (controller *VerifyUserController) Route(app *fiber.App) {
	app.Get("/api/auth/verifyEmail/:token", controller.VerifyEmail)
}

func (controller *VerifyUserController) VerifyEmail(ctx *fiber.Ctx) error {
	token := ctx.Params("token")

	request := dto.VerifyEmailRequest{
		Token: token,
	}

	response := controller.VerifyUserService.VerifyEmail(request)

	return ctx.Status(constants.HTTP_STATUS_OK_CODE).JSON(dto.GeneralResponse{
		Code:   constants.HTTP_STATUS_OK_CODE,
		Status: constants.HTTP_STATUS_OK,
		Data:   response,
	})
}
