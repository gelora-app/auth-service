package utils

import (
	"math/rand"
	"time"

	"gitlab.com/gelora-app/auth-service/pkg/constants"
)

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func GenerateTokenEmailVerification() string {
	b := make([]byte, 50)
	for i := range b {
		b[i] = constants.CHARSET[seededRand.Intn(len(constants.CHARSET))]
	}

	return string(b)
}
