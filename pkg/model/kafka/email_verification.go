package model_kafka

type PublishEmailVerificationRequest struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	Token    string `json:"token"`
}
