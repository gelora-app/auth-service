package model_postgres

import "time"

type InsertSignUpUserWithEmailRequest struct {
	FullName string
	Email    string
	Password string
}

type CheckUserByEmailRequest struct {
	Email string
}

type GetDetailUserByEmailRequest struct {
	Email string
}

type GetDetailUserByEmailResponse struct {
	ID                  string
	FullName            string
	Email               string
	PhoneNumber         string
	Password            string
	VerifiedEmail       bool
	VerifiedPhoneNumber bool
	CreatedAt           time.Time
	UpdatedAt           time.Time
}

type UpdateVerifiedEmailUserByIdRequest struct {
	ID string
}

type UpdateVerifiedEmailUserByIdResponse struct {
	VerifiedEmail bool
	UpdatedAt     *time.Time
}
