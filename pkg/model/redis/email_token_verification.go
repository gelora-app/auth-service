package model_redis

type SetTokenEmailVerificationRequest struct {
	Email string
	Token string
}

type GetTokenEmailVerificationByTokenRequest struct {
	Token string
}

type GetTokenEmailVerificationByTokenResponse struct {
	Email string
}

type DeleteTokenEmailVerificationByTokenRequest struct {
	Token string
}
