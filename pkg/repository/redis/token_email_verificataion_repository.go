package redis

import (
	modelRedis "gitlab.com/gelora-app/auth-service/pkg/model/redis"
)

type TokenEmailVerificationRepository interface {
	SetTokenEmailVerification(request modelRedis.SetTokenEmailVerificationRequest)
	GetTokenEmailVerificationByToken(request modelRedis.GetTokenEmailVerificationByTokenRequest) (response modelRedis.GetTokenEmailVerificationByTokenResponse, err error)
	DeleteTokenEmailVerificationByToken(request modelRedis.DeleteTokenEmailVerificationByTokenRequest)
}
