package redis_impl

import (
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/gelora-app/auth-service/pkg/config"
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	modelRedis "gitlab.com/gelora-app/auth-service/pkg/model/redis"
	repoRedis "gitlab.com/gelora-app/auth-service/pkg/repository/redis"
)

type TokenEmailVerificationRepositoryImpl struct {
	redis *redis.Client
}

func NewTokenEmailVerification(redis *redis.Client) repoRedis.TokenEmailVerificationRepository {
	return &TokenEmailVerificationRepositoryImpl{
		redis,
	}
}

func (repository *TokenEmailVerificationRepositoryImpl) SetTokenEmailVerification(request modelRedis.SetTokenEmailVerificationRequest) {
	ctx, cancel := config.NewRedisContext()
	defer cancel()

	err := repository.redis.Set(ctx, request.Token, request.Email, 5*time.Minute)
	exception.Panic(err.Err())
}

func (repository *TokenEmailVerificationRepositoryImpl) GetTokenEmailVerificationByToken(request modelRedis.GetTokenEmailVerificationByTokenRequest) (response modelRedis.GetTokenEmailVerificationByTokenResponse, err error) {
	ctx, cancel := config.NewRedisContext()
	defer cancel()

	result, err := repository.redis.Get(ctx, request.Token).Result()
	if err == redis.Nil {
		return modelRedis.GetTokenEmailVerificationByTokenResponse{}, constants.EmailTokenNotFoundError
	} else if err != nil {
		panic(err)
	}

	response = modelRedis.GetTokenEmailVerificationByTokenResponse{
		Email: result,
	}

	return response, nil
}

func (repository *TokenEmailVerificationRepositoryImpl) DeleteTokenEmailVerificationByToken(request modelRedis.DeleteTokenEmailVerificationByTokenRequest) {
	ctx, cancel := config.NewRedisContext()
	defer cancel()

	repository.redis.Del(ctx, request.Token)
}
