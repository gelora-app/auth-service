package postgres_impl

import (
	"errors"
	"time"

	"gitlab.com/gelora-app/auth-service/pkg/config"
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/entity"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	modelPostgres "gitlab.com/gelora-app/auth-service/pkg/model/postgres"
	repoPostgres "gitlab.com/gelora-app/auth-service/pkg/repository/postgres"
	"gorm.io/gorm"
)

type UserRepositoryImpl struct {
	postgres *gorm.DB
}

func NewUserRepository(postgres *gorm.DB) repoPostgres.UserRepository {
	return &UserRepositoryImpl{
		postgres,
	}
}

func (repository *UserRepositoryImpl) InsertSignUpUserWithEmail(request modelPostgres.InsertSignUpUserWithEmailRequest) {
	ctx, cancel := config.NewPostgresContext()
	defer cancel()

	user := entity.User{
		FullName: request.FullName,
		Email:    request.Email,
		Password: request.Password,
	}

	err := repository.postgres.WithContext(ctx).Create(&user).Error
	exception.Panic(err)
}

func (repository *UserRepositoryImpl) CheckUserByEmail(request modelPostgres.CheckUserByEmailRequest) (response bool) {
	ctx, cancel := config.NewPostgresContext()
	defer cancel()

	var entityUser entity.User

	condition := entity.User{
		Email: request.Email,
	}

	err := repository.postgres.WithContext(ctx).Where(&condition).Take(&entityUser).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false
		}
		panic(err)
	}

	response = true
	return response
}

func (repository *UserRepositoryImpl) GetDetailUserByEmail(request modelPostgres.GetDetailUserByEmailRequest) (response modelPostgres.GetDetailUserByEmailResponse, err error) {
	ctx, cancel := config.NewPostgresContext()
	defer cancel()

	var entityUser entity.User

	condition := entity.User{
		Email: request.Email,
	}

	err = repository.postgres.WithContext(ctx).Where(&condition).Take(&entityUser).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return modelPostgres.GetDetailUserByEmailResponse{}, constants.UserNotFoundError
		}
		panic(err)
	}

	response = modelPostgres.GetDetailUserByEmailResponse{
		ID:                  entityUser.GeneralEntity.ID,
		FullName:            entityUser.FullName,
		Email:               entityUser.Email,
		PhoneNumber:         entityUser.PhoneNumber,
		Password:            entityUser.Password,
		VerifiedEmail:       entityUser.VerifiedEmail,
		VerifiedPhoneNumber: entityUser.VerifiedPhoneNumber,
		CreatedAt:           entityUser.GeneralEntity.CreatedAt,
		UpdatedAt:           entityUser.GeneralEntity.UpdatedAt,
	}

	return response, nil
}

func (repository *UserRepositoryImpl) UpdateVerifiedEmailUserById(request modelPostgres.UpdateVerifiedEmailUserByIdRequest) (response modelPostgres.UpdateVerifiedEmailUserByIdResponse) {
	ctx, cancel := config.NewPostgresContext()
	defer cancel()

	condition := entity.User{
		GeneralEntity: entity.GeneralEntity{
			ID: request.ID,
		},
	}

	now := time.Now()

	updateVerifiedEmailUserRequest := map[string]interface{}{
		"verified_email": true,
		"updated_at":     &now,
	}

	result := repository.postgres.WithContext(ctx).Model(&condition).Updates(updateVerifiedEmailUserRequest)
	exception.Panic(result.Error)

	response = modelPostgres.UpdateVerifiedEmailUserByIdResponse{
		VerifiedEmail: true,
		UpdatedAt:     &now,
	}

	return response
}
