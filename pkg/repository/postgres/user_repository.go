package postgres

import (
	modelPostgres "gitlab.com/gelora-app/auth-service/pkg/model/postgres"
)

type UserRepository interface {
	InsertSignUpUserWithEmail(request modelPostgres.InsertSignUpUserWithEmailRequest)
	CheckUserByEmail(request modelPostgres.CheckUserByEmailRequest) (response bool)
	GetDetailUserByEmail(request modelPostgres.GetDetailUserByEmailRequest) (response modelPostgres.GetDetailUserByEmailResponse, err error)
	UpdateVerifiedEmailUserById(request modelPostgres.UpdateVerifiedEmailUserByIdRequest) (response modelPostgres.UpdateVerifiedEmailUserByIdResponse)
}
