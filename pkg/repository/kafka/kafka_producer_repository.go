package kafka

type KafkaProducerRepository interface {
	Publish(topic string, message string) error
}
