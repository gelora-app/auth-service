package kafka_impl

import (
	"log"

	"github.com/Shopify/sarama"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	repoKafka "gitlab.com/gelora-app/auth-service/pkg/repository/kafka"
)

type KafkaProducerRepositoryImpl struct {
	kafka sarama.SyncProducer
}

func NewKafkaProducerRepositoryImpl(kafka sarama.SyncProducer) repoKafka.KafkaProducerRepository {
	return &KafkaProducerRepositoryImpl{
		kafka,
	}
}

func (repository *KafkaProducerRepositoryImpl) Publish(topic string, message string) error {
	kafkaMessage := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}

	partition, offset, err := repository.kafka.SendMessage(kafkaMessage)
	exception.Panic(err)

	log.Printf("Send message success, Topic %v, Partition %v, Offset %d", topic, partition, offset)

	return nil
}
