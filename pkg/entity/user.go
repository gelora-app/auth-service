package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	GeneralEntity       GeneralEntity `gorm:"embedded"`
	FullName            string        `gorm:"column:full_name"`
	Email               string        `gorm:"column:email"`
	PhoneNumber         string        `gorm:"column:phone_number"`
	Password            string        `gorm:"column:password"`
	VerifiedEmail       bool          `gorm:"column:verified_email"`
	VerifiedPhoneNumber bool          `gorm:"column:verified_phone_number"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.GeneralEntity.ID = uuid.NewString()
	u.GeneralEntity.CreatedAt = time.Now()
	u.GeneralEntity.UpdatedAt = time.Now()
	return
}
