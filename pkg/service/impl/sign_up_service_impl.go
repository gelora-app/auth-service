package service_impl

import (
	"encoding/json"

	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	modelKafka "gitlab.com/gelora-app/auth-service/pkg/model/kafka"
	modelPostgres "gitlab.com/gelora-app/auth-service/pkg/model/postgres"
	modelRedis "gitlab.com/gelora-app/auth-service/pkg/model/redis"
	repoKafka "gitlab.com/gelora-app/auth-service/pkg/repository/kafka"
	repoPostgres "gitlab.com/gelora-app/auth-service/pkg/repository/postgres"
	repoRedis "gitlab.com/gelora-app/auth-service/pkg/repository/redis"
	"gitlab.com/gelora-app/auth-service/pkg/service"
	"gitlab.com/gelora-app/auth-service/pkg/utils"
	"gitlab.com/gelora-app/auth-service/pkg/validation"
	"golang.org/x/crypto/bcrypt"
)

type SignUpServiceImpl struct {
	UserPostgresRepository                repoPostgres.UserRepository
	TokenEmailVerificationRedisRepository repoRedis.TokenEmailVerificationRepository
	KafkaProducerRepository               repoKafka.KafkaProducerRepository
}

func NewSignUpService(userPostgresRepository *repoPostgres.UserRepository, tokenEmailVerificationRedisRepository *repoRedis.TokenEmailVerificationRepository, kafkaProducerRepository *repoKafka.KafkaProducerRepository) service.SignUpService {
	return &SignUpServiceImpl{
		UserPostgresRepository:                *userPostgresRepository,
		TokenEmailVerificationRedisRepository: *tokenEmailVerificationRedisRepository,
		KafkaProducerRepository:               *kafkaProducerRepository,
	}
}

func (service *SignUpServiceImpl) SignUpWithEmail(request dto.SignUpWithEmailRequest) (response dto.SignUpWithEmailResponse) {
	validation.SignUpWithEmailValidate(request)

	checkUserByEmailRequest := modelPostgres.CheckUserByEmailRequest{
		Email: request.Email,
	}

	isUser := service.UserPostgresRepository.CheckUserByEmail(checkUserByEmailRequest)
	if isUser {
		exception.Panic(constants.UserAlreadyExistsWithThisEmailError)
	}

	password := []byte(request.Password)
	hashPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	exception.Panic(err)

	insertSignUpUserWithEmailRequest := modelPostgres.InsertSignUpUserWithEmailRequest{
		FullName: request.FullName,
		Email:    request.Email,
		Password: string(hashPassword),
	}

	service.UserPostgresRepository.InsertSignUpUserWithEmail(insertSignUpUserWithEmailRequest)

	tokenEmailVerification := utils.GenerateTokenEmailVerification()

	setTokenEmailVerificationRequest := modelRedis.SetTokenEmailVerificationRequest{
		Email: request.Email,
		Token: tokenEmailVerification,
	}

	service.TokenEmailVerificationRedisRepository.SetTokenEmailVerification(setTokenEmailVerificationRequest)

	publishSendEmailVerificationRequest := modelKafka.PublishEmailVerificationRequest{
		FullName: request.FullName,
		Email:    request.Email,
		Token:    tokenEmailVerification,
	}

	publishSendEmailVerificationRequestJSON, err := json.Marshal(&publishSendEmailVerificationRequest)
	exception.Panic(err)

	service.KafkaProducerRepository.Publish(constants.TOPIC_SEND_EMAIL_VERIFICATION, string(publishSendEmailVerificationRequestJSON))

	response = dto.SignUpWithEmailResponse{
		FullName: insertSignUpUserWithEmailRequest.FullName,
		Email:    insertSignUpUserWithEmailRequest.Email,
	}

	return response
}
