package service_impl

import (
	"gitlab.com/gelora-app/auth-service/pkg/constants"
	"gitlab.com/gelora-app/auth-service/pkg/dto"
	"gitlab.com/gelora-app/auth-service/pkg/exception"
	modelPostgres "gitlab.com/gelora-app/auth-service/pkg/model/postgres"
	modelRedis "gitlab.com/gelora-app/auth-service/pkg/model/redis"
	repoPostgres "gitlab.com/gelora-app/auth-service/pkg/repository/postgres"
	repoRedis "gitlab.com/gelora-app/auth-service/pkg/repository/redis"
	"gitlab.com/gelora-app/auth-service/pkg/service"
)

type VerifyUserServiceImpl struct {
	UserPostgresRepository                repoPostgres.UserRepository
	TokenEmailVerificationRedisRepository repoRedis.TokenEmailVerificationRepository
}

func NewVerifyUserService(userPostgresRepository *repoPostgres.UserRepository, tokenEmailVerificationRedisRepository *repoRedis.TokenEmailVerificationRepository) service.VerifyUserService {
	return &VerifyUserServiceImpl{
		UserPostgresRepository:                *userPostgresRepository,
		TokenEmailVerificationRedisRepository: *tokenEmailVerificationRedisRepository,
	}
}

func (service *VerifyUserServiceImpl) VerifyEmail(request dto.VerifyEmailRequest) (response dto.VerifyEmailResponse) {
	// Get token in redis
	getTokenEmailVerificationByTokenRedisRequest := modelRedis.GetTokenEmailVerificationByTokenRequest{
		Token: request.Token,
	}

	resultGetTokenEmailVerificationRedis, err := service.TokenEmailVerificationRedisRepository.GetTokenEmailVerificationByToken(getTokenEmailVerificationByTokenRedisRequest)
	exception.Panic(err)

	// Get detail user by email in postgres
	getDetailUserByEmailPostgresRequest := modelPostgres.GetDetailUserByEmailRequest{
		Email: resultGetTokenEmailVerificationRedis.Email,
	}

	resultGetDetailUserByEmailPostgres, err := service.UserPostgresRepository.GetDetailUserByEmail(getDetailUserByEmailPostgresRequest)
	exception.Panic(err)

	// Compare
	if resultGetTokenEmailVerificationRedis.Email != resultGetDetailUserByEmailPostgres.Email {
		panic(constants.InvalidEmailTokenError)
	}

	// Update column verified email
	updateVerifiedEmailUserByIdPostgresRequest := modelPostgres.UpdateVerifiedEmailUserByIdRequest{
		ID: resultGetDetailUserByEmailPostgres.ID,
	}

	resultUpdateVerifiedEmailUserByIdPostgres := service.UserPostgresRepository.UpdateVerifiedEmailUserById(updateVerifiedEmailUserByIdPostgresRequest)

	// Delete token in redis
	deleteTokenEmailVerificationByTokenRedisRequest := modelRedis.DeleteTokenEmailVerificationByTokenRequest{
		Token: request.Token,
	}

	service.TokenEmailVerificationRedisRepository.DeleteTokenEmailVerificationByToken(deleteTokenEmailVerificationByTokenRedisRequest)

	response = dto.VerifyEmailResponse{
		VerifiedEmail: resultUpdateVerifiedEmailUserByIdPostgres.VerifiedEmail,
		UpdatedAt:     *resultUpdateVerifiedEmailUserByIdPostgres.UpdatedAt,
	}

	return response
}
