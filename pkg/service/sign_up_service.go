package service

import "gitlab.com/gelora-app/auth-service/pkg/dto"

type SignUpService interface {
	SignUpWithEmail(request dto.SignUpWithEmailRequest) (response dto.SignUpWithEmailResponse)
}
