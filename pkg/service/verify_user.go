package service

import "gitlab.com/gelora-app/auth-service/pkg/dto"

type VerifyUserService interface {
	VerifyEmail(request dto.VerifyEmailRequest) (response dto.VerifyEmailResponse)
}
