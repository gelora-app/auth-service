docker-build:
	@echo "--- Start Build Dockerfile ---"
	@docker build -t auth-service:1.0.0 .

auth-service-prod:
	@echo "--- Start Auth Service Production ---"
	docker-build
	@go run main.go auth-service

auth-service-dev:
	@echo "--- Start Auth Service Development ---"
	@go run main.go auth-service

migrate-create-pg:
	@echo "--- Generate SQL Table ---"
	@migrate create -ext sql -dir ./db/migrations -format "unix" $(name)

migrate-up:
	@echo "--- Migrate Up ---"
	@migrate -database postgresql://postgres:afterlife123@localhost/auth_service -path ./db/migrations up